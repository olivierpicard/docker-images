#!/bin/bash

## This script build a flask image on the local system

dirpath=$(dirname $(which $0))
cd "$dirpath"/..

docker build -t flask .
docker tag flask eu.gcr.io/brushed-charts/flask
docker push eu.gcr.io/brushed-charts/flask
